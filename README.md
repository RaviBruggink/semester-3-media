## Project Title
In this repository you will find some projects I made in my third semester at Fontys university of applied sciences ICT & Media. The projects are not always complete. This repository purely exists to demonstrate and track my development capabilities over the years.

## Installation
To install and use this project just download the repository and run it with any hosting service (online or local).
I did this with Visual Studio Code + the Live Server plugin.

## Install for development
After cloning the repository all you have to do is open the project in your code editor.

## Usage
[Guide on how to use the project, including examples and command-line options if applicable]


## Contributing
Feel free to use this repository as you wish. The main goal is to show my work and code structure.

## Project status
This project started in 09/2022 and ended in 02/2022