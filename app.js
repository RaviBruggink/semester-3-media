const items = document.getElementsByClassName("item");
//window.location.href.includes('design')

showTag = (event, tag) => {
    // console.log(window.location.hash)
    window.scrollBy(0,600);
    console.log('showing... ', tag);
    for (let i = 0; i < items.length; i++) {
        if (items[i].dataset.tags.includes(tag)) {
            items[i].style.display = "flex";
        } else {
            items[i].style.display = "none";
        }
    }
}

let designButton = document.getElementById("designBtn")
let developmentButton = document.getElementById("developmentBtn")
let communicationButton = document.getElementById("communicationBtn")
let proIdButton = document.getElementById("pro-idBtn")
let researchButton = document.getElementById("researchBtn")
let interactiveButton = document.getElementById("interactive-mediaBtn")
let allButton = document.getElementById("allBtn")
let fakeButton = document.getElementById("fakeButton")
let buttonArray = [designButton, developmentButton, communicationButton, proIdButton, researchButton, interactiveButton, allButton, fakeButton]

allButton.addEventListener("click", (event) => showTag(event, 'all'));
researchButton.addEventListener("click", (event) => showTag(event, 'research'));
interactiveButton.addEventListener("click", (event) => showTag(event, 'interactive-media'));
proIdButton.addEventListener("click", (event) => showTag(event, 'pro-id'));
designButton.addEventListener("click", (event) => showTag(event, 'design'));
developmentButton.addEventListener("click", (event) => showTag(event, 'development'));
communicationButton.addEventListener("click", (event) => showTag(event, 'communication'));

buttonArray.forEach(button => {
    button.addEventListener('click', () => {
        buttonArray.forEach(selectedButton => {
                if (selectedButton.classList.contains('active')) {
                    selectedButton.classList.remove('active')
                } else {
                    button.classList.add('active')
                    
                }
            }
        )
        })


})

// var corsor = document.getElementById('cursor');
//
// document.addEventListener('mousemove', function (e){
//     var x = e.layerX;
//     var y = e.layerY;
//     cursor.style.left = x + "px";
//     cursor.style.top = y + "px";
//
// })

var vid = document.getElementById('video');
vid.volume = 0.5;