var a;
var bMusic = new Audio();
bMusic.src="assets/xFilesAssets/xFilesMusic.mp3";
const button = document.querySelector('#start-button');
const menuSection = document.querySelector('#menu-section');
const introSection = document.querySelector('#intro-section');
const saucerSection = document.querySelector('#saucer-section');
const chartSection = document.querySelector('#chart-section');
const fbiSection = document.querySelector('#fbi-section');
const stormSection = document.querySelector('#storm-section');

button.addEventListener('click', (event) => {
    console.log("hello")
    console.log(menuSection);
    bMusic.play();
    menuSection.classList.add("hide");
    introSection.classList.remove("hide")
    setTimeout(() => {
        introSection.classList.add("hide");
        saucerSection.classList.remove("hide")
    }, 3000)
    setTimeout(() => {
        saucerSection.classList.add("hide");
        chartSection.classList.remove("hide")
    }, 6000)
    setTimeout(() => {
        chartSection.classList.add("hide");
        fbiSection.classList.remove("hide")
    }, 9000)
    setTimeout(() => {
        fbiSection.classList.add("hide");
        stormSection.classList.remove("hide")
    }, 12000)
    setTimeout(() => {
        bMusic.pause()
    }, 15000)
});

