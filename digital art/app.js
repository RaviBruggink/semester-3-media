const text = document.querySelectorAll("div.text");
//makes a variable from the contents of all text in the class "text" AND only when they are a div

text.forEach(function(element) {
    let tick = 1;
    let value = element.dataset.speed;
    element.innerHTML += element.innerHTML;
    element.innerHTML += element.innerHTML;

    const innerTags = element.querySelectorAll("div.inner");
    innerTags.forEach((inner, index) => {
        inner.style.left = inner.offsetWidth * index + "px";
    });
    //For each "inner" element the code sets the "left" CSS property to the width of the element multiplied by "index".

    const ticker = function() {
        tick += parseInt(value);
        //element.innerHTML = tick;
        //element.style.left = tick + "px";
        innerTags.forEach((inner, index) => {
            let width = inner.offsetWidth;
            let normalizedTextX = ((tick % width) + width) % width;
            let pos = width * (index - 1) + normalizedTextX;

            inner.style.left = pos + "px";
        });
        requestAnimationFrame(ticker);
        //Causes "ticker" to be initiated again creating a loop
    };
    ticker();
});



gsap.set(".ball", {xPercent: -50, yPercent: -50});

const ball = document.querySelector(".ball");
const pos = { x: window.innerWidth / 2, y: window.innerHeight / 2 };
const mouse = { x: pos.x, y: pos.y };

const speed = 0.3;
//makes a variable that contains and controlls the speed of the follower so that it has a delay of .7 in this case

const xSet = gsap.quickSetter(ball, "x", "px");
const ySet = gsap.quickSetter(ball, "y", "px");
//makes a variable of the mouse position x and y

window.addEventListener("mousemove", e => {    
  mouse.x = e.x;
  mouse.y = e.y;  
});

gsap.ticker.add(() => {
  
  // change speed for higher refresh rate
  const dt = 1.0 - Math.pow(1.0 - speed, gsap.ticker.deltaRatio()); 
  
  pos.x += (mouse.x - pos.x) * dt;
  pos.y += (mouse.y - pos.y) * dt;
  xSet(pos.x);
  ySet(pos.y);
});